import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";


// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//       <Nav />
//       <div className="container">
//         <LocationForm />
//         {/* <ConferenceForm />
//         <AttendConferenceForm />
//         <PresentationForm />
//         <AttendeesList attendees={props.attendees} /> */}
//       </div>
//     </>
//   );
// }

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="conference/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="presentations/new" element={<PresentationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
